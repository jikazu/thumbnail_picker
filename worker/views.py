import base64
import time
from random import random
import imageio
import cv2
import numpy as np
import torch
import torchvision.models
import torch as t
from datetime import datetime
from torchvision import transforms
from PIL import Image
from django.http import FileResponse, HttpResponse, HttpResponseServerError
from io import BytesIO

highest_record = {"score": 0, "image": ''}
frames = []


def get_thumbnail(request):
    # start = time.time()
    
    captureFrames()
    numpy_image = analyzeImages()
    image = cv2.cvtColor(numpy_image, cv2.COLOR_BGR2RGB)
    image = Image.fromarray(image)
    buff = BytesIO()
    image.save(buff, format="JPEG")
    imageBase64 = base64.b64encode(buff.getvalue()).decode("utf-8")
    imgHtml = f"<img src='data:image/png;base64,{imageBase64}' />"
    # duration = (time.time() - start) * 1000
    # print(f'analyzing takes {(duration/1000)%60} sec')
    return HttpResponse(imgHtml)


def captureFrames():
    cap = cv2.VideoCapture("video/2.mp4")
    currentFrame = 0
    while(currentFrame >= 0):
        ret, frame = cap.read()
        if (frame is None):
            currentFrame = -1
            cap.release()
            cv2.destroyAllWindows()
        else:
            image = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            is_sharp = variance_of_laplacian(image)
            is_bright = isbright(frame)
            print(f'frame is_sharp:{is_sharp} and is_bright:{is_bright}')
            if is_sharp and is_bright:
                frames.append(frame)

            currentFrame += 1


def analyzeImages():
    def prepare_image(image):
        if image.mode != 'RGB':
            image = image.convert("RGB")
        Transform = transforms.Compose([
            transforms.Resize([64, 64]),
            transforms.ToTensor(),
        ])
        image = Transform(image)
        image = image.unsqueeze(0)
        return image

    def predict(image, model):
        image = prepare_image(image)
        with torch.no_grad():
            preds = model(image)
        score = preds.detach().numpy().item()
        score = str(round(score, 2))
        if (float(score) > float(highest_record["score"])):
            highest_record.update({"score": score, "image": x})

    for x in frames:
        image = cv2.cvtColor(x, cv2.COLOR_BGR2RGB)
        image = Image.fromarray(image)

        model = torchvision.models.resnet50(pretrained=True)
        model.fc = torch.nn.Linear(in_features=2048, out_features=1)
        #   model.load_state_dict(torch.load('model/model-resnet50.pth'))
        model.eval()
        predict(image, model)

    img = highest_record.get("image")
    return img


def variance_of_laplacian(image):
    # compute the Laplacian of the image and then return the focus
    # measure, which is simply the variance of the Laplacian
    return cv2.Laplacian(image, cv2.CV_64F).var() > 100


def isbright(image, dim=224, thresh=0.3):
    L, A, B = cv2.split(cv2.cvtColor(image, cv2.COLOR_BGR2LAB))
    L = L/np.max(L)
    return np.mean(L) > thresh