from importlib.resources import path
from . import views

urlpatterns = [
  path('', views.get_thumbnail)
]